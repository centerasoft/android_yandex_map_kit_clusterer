package com.resonancelabllc.yandexclustering;

import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import ru.yandex.yandexmapkit.utils.ScreenPoint;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ryashentsev
 * Date: 01.08.13
 * Time: 13:51
 * To change this template use File | Settings | File Templates.
 */
public class YandexMapClusterer {

    private static List<Object> sLastClusteredObjects;

    /**
     * Joins geopoints to clusters.
     * @param points geo point to cluster
     * @param standardPinImageParameters parameters of standard pin image
     * @param clusterPinImageParameters parameters of cluster pin image
     * @param mapController yandex map controller
     * @return return array of geo points and clusters
     */
    public static List<Object> clusterPoints(List<GeoPoint> points, PinImageParameters standardPinImageParameters, PinImageParameters clusterPinImageParameters, MapController mapController){
        return clasterObjects(new ArrayList<Object>(points),
                standardPinImageParameters,
                clusterPinImageParameters,
                mapController);
    }

    private static List<Object> clasterObjects(List<Object> objects, PinImageParameters standardPinImageParameters, PinImageParameters clusterPinImageParameters, MapController mapController){
        //GC hack
        sLastClusteredObjects = new ArrayList<Object>(objects);
        for(int i=0;i<sLastClusteredObjects.size();i++){
            List<Object> intersectObjects = isNearToAny(sLastClusteredObjects, sLastClusteredObjects.get(i), standardPinImageParameters, clusterPinImageParameters, mapController);
            if(intersectObjects.size()>0){
                intersectObjects.add(sLastClusteredObjects.get(i));
                sLastClusteredObjects.removeAll(intersectObjects);
                sLastClusteredObjects.add(new Cluster(intersectObjects));
                return clasterObjects(sLastClusteredObjects, standardPinImageParameters, clusterPinImageParameters, mapController);
            }
        }
        return sLastClusteredObjects;
    }

    private static List<Object> isNearToAny(List<Object> points, Object object, PinImageParameters standardPinImageParameters, PinImageParameters clusterPinImageParameters, MapController mapController){
        List<Object> intersections = new ArrayList<Object>();
        ScreenPoint sp;
        ScreenPoint screenPoint = getScreenPoint(object, mapController);
        PinImageParameters parameters = (object instanceof Cluster)?clusterPinImageParameters:standardPinImageParameters;
        PinImageParameters params;

        for(Object obj: points){
            if(obj==object) continue;
            sp = getScreenPoint(obj, mapController);
            params = (obj instanceof Cluster)?clusterPinImageParameters:standardPinImageParameters;

            if( intersects(screenPoint, sp, parameters, params)){
                intersections.add(obj);
            }
        }
        return intersections;
    }

    private static boolean intersects(ScreenPoint sp1, ScreenPoint sp2, PinImageParameters params1, PinImageParameters params2){
        float minX1 = sp1.getX()+params1.getOffsetX()-params1.getWidth()/2;
        float maxX1 = sp1.getX()+params1.getOffsetX()+params1.getWidth()/2;
        float minX2 = sp2.getX()+params2.getOffsetX()-params2.getWidth()/2;
        float maxX2 = sp2.getX()+params2.getOffsetX()+params2.getWidth()/2;

        float minY1 = sp1.getY()+params1.getOffsetY()-params1.getHeight()/2;
        float maxY1 = sp1.getY()+params1.getOffsetY()+params1.getHeight()/2;
        float minY2 = sp2.getY()+params2.getOffsetY()-params2.getHeight()/2;
        float maxY2 = sp2.getY()+params2.getOffsetY()+params2.getHeight()/2;

        if(((minX1>=minX2 && minX1<=maxX2) || (maxX1>=minX2 && maxX1<=maxX2)) && ((minY1>=minY2 && minY1<=maxY2) || (maxY1>=minY2 && maxY1<=maxY2)) ){
            return true;
        }
        if(((minX2>=minX1 && minX2<=maxX1) || (maxX2>=minX1 && maxX2<=maxX1)) && ((minY2>=minY1 && minY2<=maxY1) || (maxY2>=minY1 && maxY2<=maxY1)) ){
            return true;
        }
        return false;
    }

    private static ScreenPoint getScreenPoint(Object object, MapController mapController){
        if(object instanceof GeoPoint){
            return mapController.getScreenPoint( (GeoPoint)object );
        }else if(object instanceof Cluster){
            return mapController.getScreenPoint( ((Cluster)object).getCenter() );
        }
        return null;
    }

    /**
     * Cluster class. May be return in objects list after clusterPoints calling.
     */
    public static class Cluster{
        private List<GeoPoint> mPoints = new ArrayList<GeoPoint>();
        public Cluster(List<Object> objects){
            if(objects!=null){
                for(Object obj: objects){
                    addObject(obj);
                }
            }
        }

        /**
         * Returns array of geo points joined in cluster
         * @return
         */
        public List<GeoPoint> getPoints(){
            return mPoints;
        }
        public void addObject(Object object){
            if(mPoints.indexOf(object)==-1){
                if(object instanceof GeoPoint){
                    mPoints.add((GeoPoint)object);
                }else if(object instanceof Cluster){
                    mPoints.addAll( ((Cluster)object).getPoints() );
                }
            }
        }

        /**
         * Returns center geo point of cluster
         * @return
         */
        public GeoPoint getCenter(){
            if(mPoints.size()>0){
                double lat = 0;
                double lon = 0;
                for(GeoPoint p: mPoints){
                    lat+=p.getLat();
                    lon+=p.getLon();
                }
                return new GeoPoint(lat/mPoints.size(), lon/mPoints.size());
            }
            return null;
        }
    }

    /**
     * Pin image parameters class
     */
    public static class PinImageParameters{
        private int mOffsetX;
        private int mOffsetY;
        private int mWidth;
        private int mHeight;

        /**
         *
         * @param offsetX X offset. Equals to offset in overlay item
         * @param offsetY Y offset. Equals to offset in overlay item
         * @param width pin image width
         * @param height pin image height
         */
        public PinImageParameters(int offsetX, int offsetY, int width, int height){
            mOffsetX = -offsetX;
            mOffsetY = -offsetY;
            mWidth = width;
            mHeight = height;
        }

        /**
         * Returns X offset. Equals to offset in overlay item
         * @return
         */
        public int getOffsetX(){
            return mOffsetX;
        }
        /**
         * Returns Y offset. Equals to offset in overlay item
         * @return
         */
        public int getOffsetY(){
            return mOffsetY;
        }
        /**
         * Returns pin image width
         * @return
         */
        public int getWidth(){
            return mWidth;
        }
        /**
         * Returns pin image height
         * @return
         */
        public int getHeight(){
            return mHeight;
        }
    }

}

package com.example.yandex_clustering_sample;

import android.app.Activity;
import android.graphics.*;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import com.resonancelabllc.yandexclustering.YandexMapClusterer;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.map.MapEvent;
import ru.yandex.yandexmapkit.map.OnMapListener;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.utils.GeoPoint;

import java.util.ArrayList;
import java.util.List;

public class MyActivity extends Activity implements OnMapListener {

    MapController mMapController;
    OverlayManager mOverlayManager;
    private Overlay mOverlay;

    private List<GeoPoint> mPoints = new ArrayList<GeoPoint>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        final MapView mapView = (MapView) findViewById(R.id.map);

        mMapController = mapView.getMapController();
        mMapController.setZoomCurrent(12);
        mMapController.addMapListener(this);
        mOverlayManager = mMapController.getOverlayManager();
        mOverlay = new Overlay(mMapController);
        mOverlayManager.addOverlay(mOverlay);

        // Disable determining the user's location
        mOverlayManager.getMyLocation().setEnabled(false);


        //create geo point to show
        mPoints.add(new GeoPoint(55.752004, 37.617017));
        mPoints.add(new GeoPoint(58.734029, 38.588499));
        mPoints.add(new GeoPoint(65.734029, 39.588499));
        mPoints.add(new GeoPoint(56.734029, 37.588499));
        mPoints.add(new GeoPoint(77.734029, 37.588499));
        mPoints.add(new GeoPoint(52.734029, 36.588499));
        mPoints.add(new GeoPoint(85.734029, 35.588499));
        mPoints.add(new GeoPoint(50.734029, 37.588499));
        mPoints.add(new GeoPoint(93.734029, 37.588499));

        showObjects();

        mMapController.setPositionAnimationTo(mPoints.get(0));
    }

    public void showObjects(){
        mOverlay.clearOverlayItems();

        //create drawables to determine pin sizes
        BitmapDrawable standardPin = (BitmapDrawable) getResources().getDrawable(R.drawable.pin);
        BitmapDrawable clusterPin = (BitmapDrawable) getResources().getDrawable(R.drawable.cluster);
        int standardPinWidth = standardPin.getBitmap().getWidth();
        int standardPinHeight = standardPin.getBitmap().getHeight();
        int clusterPinWidth = clusterPin.getBitmap().getWidth();
        int clusterPinHeight = clusterPin.getBitmap().getHeight();

        //cluster points
        List<Object> clustered = YandexMapClusterer.clusterPoints(mPoints,
                new YandexMapClusterer.PinImageParameters(0, standardPinHeight / 2, standardPinWidth, standardPinHeight),
                new YandexMapClusterer.PinImageParameters(0, 0, clusterPinWidth, clusterPinHeight),
                mMapController);

        //create overlay items and put it to overlay layer
        for(Object obj: clustered){
            GeoPoint p;
            Drawable pin;
            OverlayItem overlayItem;
            if(obj instanceof GeoPoint){
                p = (GeoPoint) obj;
                pin = getResources().getDrawable(R.drawable.pin);
                overlayItem = new OverlayItem(p, pin);
                overlayItem.setOffsetY(standardPinHeight/2);
            }else{
                //cluster object
                p = ((YandexMapClusterer.Cluster)obj).getCenter();
                pin = writeOnDrawable(R.drawable.cluster, String.valueOf(((YandexMapClusterer.Cluster)obj).getPoints().size()));
                overlayItem = new OverlayItem(p, pin);
            }

            // Add the object to the layer
            mOverlay.addOverlayItem(overlayItem);
        }
    }

    /**
     * Writes text on bitmap
     *
     * @param drawableId
     * @param text
     * @return BitmapDrawable with text
     */
    private BitmapDrawable writeOnDrawable(int drawableId, String text){

        Bitmap bm = BitmapFactory.decodeResource(getResources(), drawableId).copy(Bitmap.Config.ARGB_8888, true);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setColor(Color.WHITE);
        paint.setTextSize(40);

        Canvas canvas = new Canvas(bm);
        canvas.drawText(text, bm.getWidth()/2, bm.getHeight()/2+10, paint);

        return new BitmapDrawable(getResources(), bm);
    }

    @Override
    public void onMapActionEvent(MapEvent mapEvent) {
        Log.d("boombate", "mapEvent = "+mapEvent.getMsg());
        switch (mapEvent.getMsg()) {
            case MapEvent.MSG_SCROLL_END:
                showObjects();
                break;
            case MapEvent.MSG_SCALE_END:
                showObjects();
                break;
            case MapEvent.MSG_ZOOM_END:
                showObjects();
                break;
        }
    }
}